//Item 2
db.fruits.aggregate(
    [
        {$match: {onSale: true}},
        {$count: "fruitsOnSale"}
    ]
)

//Item  3
db.fruits.aggregate(
    [
        {$match: {stock: {$gt: 20}}},
        {$count: "enoughStock"}
    ]
)

//Item 4
db.fruits.aggregate(
    [
        {$match: {onSale: true}},
        {$group: {_id: "$supplier_id", avgPrice: {$avg: { $multiply: ["$price"]}}}}
    ]
)

//Item 5

db.fruits.aggregate(
    [
        {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}}
    ]
)

//Item 6
db.fruits.aggregate(
    [
        {$group: {_id: "$supplier_id", min_price: {$min: "$price"}}}
    ]
)
